

import sys
import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd  
#dataframe dos dimensiones
datos = {'nombre':['María', 'Luis', 'Carmen', 'Antonio'],
'edad':[18, 22, 20, 21],
'grado':['Economía', 'Medicina', 'Arquitectura', 'Economía'],
'correo':['maria@gmail.com', 'luis@yahoo.es', 'carmen@gmail.com', 'antonio@gmail.com'],
'sexo':['F', 'M', 'F', 'M']
} 

df = pd.DataFrame(datos)
print(df)
#tipos de datos
print(df.dtypes)
#muestra solo los mayores de 18
#print(df[(df['edad']>18)])
print()
#muestra ordenado por nombre
#print(df.sort_values('nombre', ascending='false'))


#TODO
#mostrar las que sean mujeres
#ordenar por grado

