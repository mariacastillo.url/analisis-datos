import sys
import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd  

df = pd.DataFrame({'Días':['L', 'M', 'X', 'J', 'V', 'S', 'D'], 
                   'Guatemala':[28.5, 30.5, 31, 30, 28, 27.5, 30.5], 
                   'Totonicapan':[24.5, 25.5, 26.5, 25, 26.5, 24.5, 25]})
fig, ax = plt.subplots()
#df.plot.bar(stacked=False)
df.plot(x = 'Días', y = 'Guatemala', ax = ax)
df.plot(x = 'Días', y = 'Totonicapan', ax = ax)
ax.grid(axis = 'y', color = 'gray', linestyle = 'dashed')
plt.show()

plt.savefig(sys.stdout.buffer)
# Mostrar el gráfico
sys.stdout.flush() 

#TODO
#agregar titulo

#agregar leyenda de filas y columnas

#agregar un departamento mas con sus temperaturas
