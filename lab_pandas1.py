#Three lines to make our compiler able to draw:
#https://www.w3schools.com/python/trypython.asp?filename=demo_matplotlib_plotting1

import sys
import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd  

#series
s = pd.Series([1, 2, 2, 3, 3, 3, 4, 4, 4, 4])
print(s.describe())

s = pd.Series({'Matemáticas': 6.0,  'Economía': 4.5, 'Programación': 8.5})
print(s.sort_values())
print(s.sort_index(ascending = False))

print()

#dataframe dos dimensiones
datos = {'nombre':['María', 'Luis', 'Carmen', 'Antonio'],
'edad':[18, 22, 20, 21],
'grado':['Economía', 'Medicina', 'Arquitectura', 'Economía'],
'correo':['maria@gmail.com', 'luis@yahoo.es', 'carmen@gmail.com', 'antonio@gmail.com'],
'sexo':['F', 'M', 'F', 'M']
} 

df = pd.DataFrame(datos)
print(df)
print(df.dtypes)
print(df[(df['edad']>18)])
print()
print(df.sort_values('nombre', ascending='false'))

# Crear la figura y los ejes
fig, ax = plt.subplots()
# Dibujar puntos
ax.scatter(x = [1, 2, 3], y = [3, 2, 1])
# Guardar el gráfico en formato png
plt.show()
#plt.savefig(sys.stdout.buffer)
# Mostrar el gráfico
#sys.stdout.flush()

#diagrama de lineas
fig, ax = plt.subplots()
ax.plot([1, 2, 3, 4], [1, 2, 0, 0.5])
plt.show()
#plt.savefig(sys.stdout.buffer)
# Mostrar el gráfico
#sys.stdout.flush()

#diagrama de areas
fig, ax = plt.subplots()
ax.fill_between([1, 2, 3, 4], [1, 2, 0, 0.5])
plt.show()
#plt.savefig(sys.stdout.buffer)
# Mostrar el gráfico
#sys.stdout.flush()

#barras
fig, ax = plt.subplots()
ax.bar([1, 2, 3], [3, 2, 1])
plt.show()
#plt.savefig(sys.stdout.buffer)
# Mostrar el gráfico
#sys.stdout.flush()

#diagrama de pie
fig, ax = plt.subplots()
ax.pie([5, 4, 3, 2, 1])
plt.show()
#plt.savefig(sys.stdout.buffer)
# Mostrar el gráfico
#sys.stdout.flush()

fig, ax = plt.subplots()
dias = ['L', 'M', 'X', 'J', 'V', 'S', 'D']
temperaturas = {'Madrid':[28.5, 30.5, 31, 30, 28, 27.5, 30.5], 'Barcelona':[24.5, 25.5, 26.5, 25, 26.5, 24.5, 25]}
ax.plot(dias, temperaturas['Madrid'], color = 'tab:purple')
ax.plot(dias, temperaturas['Barcelona'], color = 'tab:green')
ax.set_title('Evolución de la temperatura diaria', loc = "left", fontdict = {'fontsize':14, 'fontweight':'bold', 'color':'tab:blue'})
ax.set_xlabel('Dias de la semana')
ax.set_ylabel('Temperaturas')
ax.set_ylim([20,35])
ax.grid(axis = 'y', color = 'gray', linestyle = 'dashed')
plt.show()
#plt.savefig(sys.stdout.buffer)
# Mostrar el gráfico
#sys.stdout.flush()

df = pd.DataFrame({'Días':['L', 'M', 'X', 'J', 'V', 'S', 'D'], 
                   'Madrid':[28.5, 30.5, 31, 30, 28, 27.5, 30.5], 
                   'Barcelona':[24.5, 25.5, 26.5, 25, 26.5, 24.5, 25]})
fig, ax = plt.subplots()
#df.plot.bar(stacked=False)
df.plot(x = 'Días', y = 'Madrid', ax = ax)
df.plot(x = 'Días', y = 'Barcelona', ax = ax)
#df.plot.bar(stacked=False)
#df.plot.area()
plt.show()

plt.savefig(sys.stdout.buffer)
# Mostrar el gráfico
sys.stdout.flush()


