
import sys
import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd  
#barras
fig, ax = plt.subplots()
ax.bar([1, 2, 3], [3, 2, 1])
ax.set_xlabel('eje x')
ax.set_ylabel('eje y')
ax.set_title('Datos de prueba', loc = "left", fontdict = {'fontsize':14, 'fontweight':'bold', 'color':'tab:blue'})
plt.show()
plt.savefig(sys.stdout.buffer)
# Mostrar el gráfico
sys.stdout.flush()


#modificar para que sean datos de cantidad de estudiantes por edad
